package com.kizio.androidtest.view

import android.content.Context
import android.util.AttributeSet
import android.widget.LinearLayout
import com.kizio.androidtest.R
import com.kizio.androidtest.data.Fact
import kotlinx.android.synthetic.capgemini.view_fact.view.description
import kotlinx.android.synthetic.capgemini.view_fact.view.title

/**
 * Displays the contents of a [Fact].
 *
 * @author Graeme Sutherland
 * @since 08/03/2018
 * @param context The [Context] the view is being displayed in
 * @param attributes The [AttributeSet] used to style the view
 * @param defaultStyle The default style to apply to the view
 */
class FactView (context: Context?, attributes: AttributeSet?, defaultStyle: Int)
	: LinearLayout(context, attributes, defaultStyle) {

	/**
	 * Alternative constructor.
	 *
	 * @param context The [Context] the view is being displayed in
	 */
	constructor(context: Context?) : this(context, null)

	/**
	 * Alternative constructor.
	 *
	 * @param context The [Context] the view is being displayed in
	 * @param attributes The [AttributeSet] used to style the view
	 */
	constructor(context: Context?, attributes: AttributeSet?) : this(context, attributes, 0)

	/**
	 * Initialisation block.
	 */
	init {
		inflate(context, R.layout.view_fact, this)

		orientation = VERTICAL
	}

	/**
	 * Sets the [Fact] data to display.
	 *
	 * @param fact The [Fact] that contains the data to display
	 */
	public fun setFact(fact: Fact?) {
		title.text = fact?.title
		description.text = fact?.description
	}
}