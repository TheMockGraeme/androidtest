package com.kizio.androidtest

import android.content.Intent
import android.os.Bundle
import com.kizio.androidtest.adapter.FactAdapter
import com.kizio.androidtest.data.FactList
import com.kizio.androidtest.sync.CapgeminiSyncService
import com.kizio.androidtest.sync.SyncService
import kotlinx.android.synthetic.capgemini.activity_capgemini.facts_list

/**
 * The main activity for the Capgemini test app.
 *
 * @author Graeme Sutherland
 * @since 04/03/2018
 */
class CapgeminiMainActivity : MainActivity<FactList>() {

	/**
	 * Invoked when the app is created.
	 */
	override fun onCreate(savedInstanceState: Bundle?) {
		// The content view has to be setup before onCreate is called, otherwise there might be a
		// null exception when the app attempts to restore the data field.
		setContentView(R.layout.activity_capgemini)

		super.onCreate(savedInstanceState)
	}

	/**
	 * Gets the class to launch the [SyncService].
	 *
	 * @return The [CapgeminiSyncService] class that the activity uses
	 */
	override fun getSyncServiceClass(): Class<CapgeminiSyncService> {
		return CapgeminiSyncService::class.java
	}

	/**
	 * Sets the [Intent] used to start the [SyncService].
	 * <p>
	 * TODO The getSyncService method could be rolled into this.
	 * </p>
	 * @param intent The [Intent] to configure
	 */
	override fun initialiseIntent(intent: Intent) {
		// TODO The source string needs to be externalised.
		intent.putExtra(SyncService.SOURCE_TYPE, SyncService.URL)
		intent.putExtra(SyncService.SOURCE, "https://dl.dropboxusercontent.com/s/2iodh4vg0eortkl/facts.json")
	}

	/**
	 * Invoked when the data to display is set.
	 * <p>
	 * TODO Make this a part of the [setData] method?
	 * </p>
	 * @param newData The new data object to display
	 */
	override fun onSetData(newData: FactList?) {
		val facts = newData?.rows

		if (facts != null && facts_list != null) {
			facts_list.adapter = FactAdapter(this, facts)

			setTitle(newData.title)
		}
	}
}
