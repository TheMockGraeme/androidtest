package com.kizio.androidtest.data

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

/**
 * Data object that holds the facts about a subject.
 *
 * @author Graeme Sutherland
 * @since 07/03/2018
 */
@Parcelize
data class FactList (var title: String?, var rows: Array<Fact>?) : Parcelable {

	/**
	 * No argument constructor, used for deserialising JSON.
	 */
	constructor() : this(null, null)
}