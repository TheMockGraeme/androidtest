package com.kizio.androidtest.data

import android.graphics.Bitmap
import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

/**
 * Data object that holds a fact about a subject.
 *
 * @author Graeme Sutherland
 * @since 07/03/2018
 * @param title The title [String] for the fact
 * @param description The details of the fact as a [String]
 * @param imageHref A URL [String] pointing at a location of a file to download
 * @param image A [Bitmap] to display by the fact
 */
@Parcelize
data class Fact (var title: String?, var description: String?, var imageHref: String?,
				 var image: Bitmap?) : Parcelable {

	/**
	 * No argument constructor.
	 */
	constructor() : this(null, null, null, null)
}