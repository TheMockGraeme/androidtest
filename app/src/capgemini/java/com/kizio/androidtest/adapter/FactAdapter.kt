package com.kizio.androidtest.adapter

import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import com.kizio.androidtest.data.Fact
import com.kizio.androidtest.view.FactView

/**
 * Adapter to display the contents of an [Array] of [Fact] objects in a list view.
 *
 * @author Graeme Sutherland
 * @since 07/03/2018
 * @param aContext The [Context] the list is being displayed in
 * @param factList The [Array] of [Fact] objects to display in the list view
 */
class FactAdapter(aContext: Context, factList : Array<Fact>) : BaseAdapter() {

	/**
	 * The [Context] the list is being displayed in.
	 */
	private val context: Context = aContext

	/**
	 * The [Array] of [Fact] objects to display in the list view.
	 */
	private val facts: Array<Fact> = factList

	/**
	 * Gets and populates the [View] to display the data in.
	 *
	 * @param position The position of the item in the list
	 * @param convertView The [View] to reuse if possible
	 * @param parent The [ViewGroup] that this view will eventually be attached to
	 * @return A [View] corresponding to the data at the specified position.
	 */
	override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
		val factView: FactView

		if (convertView is FactView) {
			factView = convertView
		} else {
			factView = FactView(context)
		}

		factView.setFact(getItem(position))

		return factView
	}

	/**
	 * Get the [Fact] associated with the specified position in the data set.
	 *
	 * @param position Position of the item whose data we want within the adapter's data set
	 * @return The data at the specified position
	 */
	override fun getItem(position: Int): Fact {
		return facts[position]
	}

	/**
	 * Get the row id associated with the specified position in the list.
	 *
	 * @param position The position of the item within the dataset
	 * @return The id of the item at the specified position
	 */
	override fun getItemId(position: Int): Long {
		return position.toLong()
	}

	/**
	 * How many items are in the data set represented by this Adapter.
	 *
	 * @return The number of items in the adapter
	 */
	override fun getCount(): Int {
		return facts.size
	}
}