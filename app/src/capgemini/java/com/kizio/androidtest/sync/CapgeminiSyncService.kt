package com.kizio.androidtest.sync

import com.kizio.androidtest.conversion.CapgeminiConverter
import com.kizio.androidtest.data.Fact
import com.kizio.androidtest.data.FactList

/**
 * [SyncService] for the CBA code test app.
 *
 * @author Graeme Sutherland
 * @since 07/03/2018
 */
class CapgeminiSyncService : SyncService<FactList>(CapgeminiSyncService::class.java.simpleName,
		CapgeminiConverter()) {

	/**
	 * Processes the data after being downloaded. Clears out any null records in the list.
	 *
	 * @param data The [FactList] to process
	 */
	override fun process(data : FactList) {
		val rows = data.rows as Array<Fact>
		val facts = ArrayList<Fact>(rows.size)

		for (fact in rows) {
			if (fact.title != null || fact.description != null || fact.imageHref != null) {
				facts.add(fact)
			}
		}

		data.rows = facts.toTypedArray()
	}
}