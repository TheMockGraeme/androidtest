package com.kizio.androidtest.conversion

import com.kizio.androidtest.data.FactList

/**
 * [Converter] for the CBA code test app.
 *
 * @author Graeme Sutherland
 * @since 07/03/2018
 */
class CapgeminiConverter : Converter<FactList> (FactList::class.java) {
	// Empty class body.
}