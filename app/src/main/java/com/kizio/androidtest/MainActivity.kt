package com.kizio.androidtest

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.os.Parcelable
import android.support.v4.content.LocalBroadcastManager
import android.support.v7.app.AppCompatActivity
import com.kizio.androidtest.sync.SyncService

/**
 * Common activity code for all the test apps.
 * <p>
 * The generic type [T] is the data type that the app consumes to produce its display.
 * </p>
 *
 * @author Graeme Sutherland
 * @since 04/03/2018
 */
abstract class MainActivity<in T : Parcelable> : AppCompatActivity() {

	/**
	 * Companion object containing constants to be used to persist data.
	 */
	private companion object {
		/**
		 * Key for the retrieved data.
		 */
		private const val DATA = "retrieved_data"
	}

	/**
	 * Anonymous inner class that acts as a [BroadcastReceiver], used to handle messages from the
	 * [SyncService] thread.
	 */
	private val receiver = object: BroadcastReceiver() {

		/**
		 * Invoked when a message is received.
		 *
		 * @param context The [Context] the listener is operating in
		 * @param intent An [Intent] carrying the data
		 */
		override fun onReceive(context: Context, intent: Intent) {
			setData(intent.getParcelableExtra(SyncService.DATA))
		}
	}

	/**
	 * The data being displayed by the app.
	 */
	private var data: T? = null

	/**
	 * Invoked when the activity is created.
	 *
	 * @param savedInstanceState A [Bundle] containing saved state from a previous instance
	 */
	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)

		val manager = LocalBroadcastManager.getInstance(this)
		val filter = IntentFilter(SyncService.DATA_ACTION)

		manager.registerReceiver(receiver, filter)

		if (savedInstanceState != null) {
			// Call setData() as this updates the UI.
			setData(savedInstanceState.getParcelable<T?>(DATA))
		}
	}

	/**
	 * Invoked when the activity starts.
	 */
	override fun onStart() {
		super.onStart()

		// For now only sync data when the app starts, and doesn't have any set.
		if (data == null) {
			syncData()
		}
	}

	/**
	 * Invoked when the instance of the activity is destroyed. Contains cleanup code for the local
	 * broadcast receiver.
	 */
	override fun onDestroy() {
		super.onDestroy()

		val manager = LocalBroadcastManager.getInstance(this)

		manager.unregisterReceiver(receiver)
	}

	/**
	 * Invoked when the activity saves its instance state.
	 *
	 * @param outState A [Bundle] containing local data to persist
	 */
	override fun onSaveInstanceState(outState: Bundle?) {
		super.onSaveInstanceState(outState)

		if (outState != null && data != null) {
			outState.putParcelable(DATA, data)
		}
	}

	/**
	 * Sets the data displayed.
	 *
	 * @param newData The new data to display
	 */
	fun setData(newData: T?) {
		data = newData

		onSetData(newData)
	}

	/**
	 * Function called when the app is to synchronise data.
	 */
	fun syncData() {
		val intent = Intent(this, getSyncServiceClass())

		initialiseIntent(intent)

		startService(intent)
	}

	/**
	 * Gets the class to launch the [SyncService].
	 *
	 * @return The [SyncService] class that the activity uses
	 */
	protected abstract fun getSyncServiceClass() : Class<out SyncService<T>>

	/**
	 * Sets the [Intent] used to start the [SyncService].
	 * <p>
	 * TODO The getSyncService method could be rolled into this.
	 * </p>
	 * @param intent The [Intent] to configure
	 */
	protected abstract fun initialiseIntent(intent: Intent)

	/**
	 * Invoked when the data to display is set.
	 * <p>
	 * TODO Make this a part of the [setData] method?
	 * </p>
	 *
	 * @param newData The new data object to display
	 */
	protected abstract fun onSetData(newData: T?)
}
