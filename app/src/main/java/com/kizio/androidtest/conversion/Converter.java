package com.kizio.androidtest.conversion;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Generic class that handles the conversion of a {@link String} into an object using the GSON
 * library.
 *
 * @author Graeme Sutherland
 * @since 04/03/2018
 */
public abstract class Converter <T> {

	/**
	 * The {@link Class} used to output the data as.
	 */
	private final Class<T> outputClass;

	/**
	 * The {@link Gson} library endpoint used to convert the {@link String} into an object of type
	 * T.
	 */
	private final Gson gson;

	/**
	 * Constructor.
	 *
	 * @param aClass The {@link Class} being used to perform the conversion
	 */
	protected Converter (final Class<T> aClass) {
		super ();

		final GsonBuilder builder = new GsonBuilder();

		initialiseBuilder(builder);
		this.outputClass = aClass;
		this.gson = builder.create();
	}

	/**
	 * Performs the conversion of a piece of JSON in a {@link String} into the corresponding object
	 * of type T.
	 *
	 * @param json A {@link String} containing the JSON data
	 * @return An object of type T based on the supplied JSON
	 */
	public T convert (final String json) {
		return this.gson.fromJson(json, this.outputClass);
	}

	/**
	 * Custom initialisation code for an instance of a provider.
	 *
	 * @param builder The {@link GsonBuilder} being configured
	 */
	protected void initialiseBuilder(final GsonBuilder builder) {
		// Do nothing.
	}
}
