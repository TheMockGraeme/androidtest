package com.kizio.androidtest.listener

import android.view.View
import android.widget.ExpandableListView

/**
 * This listener blocks the onClick message from occurring on a group in an [ExpandableListView].
 * It's part of a pattern used to force the groups to remain open, allowing it to be used as a
 * multi-level list.
 *
 * @author Graeme Sutherland
 * @since 07/03/2018
 */
class BlockGroupClickListener : ExpandableListView.OnGroupClickListener {
	/**
	 * Callback method to be invoked when a group has been clicked. This returns true, preventing
	 * the [ExpandableListView] from collapsing it, as it thinks that the event has already been
	 * handled.
	 *
	 * @param parent The [ExpandableListView] where the click happened
	 * @param v The [View] within the expandable list/ListView that was clicked
	 * @param groupPosition The group position that was clicked
	 * @param id The row id of the group that was clicked
	 * @return Always returns true
	 */
	override fun onGroupClick(parent: ExpandableListView?, v: View?, groupPosition: Int, id: Long)
			: Boolean {
		return true
	}
}
