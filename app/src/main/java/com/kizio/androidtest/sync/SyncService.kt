package com.kizio.androidtest.sync

import android.app.IntentService
import android.content.Intent
import android.os.Parcelable
import android.support.v4.content.LocalBroadcastManager
import com.kizio.androidtest.conversion.Converter
import com.kizio.androidtest.download.Retriever

/**
 * [IntentService] that handles retrieving data from the specified URL.
 * <p>
 * The reason for using an [IntentService] is that it's separate from an Activity, and therefore can
 * continue to run if the device is rotated, and the configuration is recreated. An AsyncTask has to
 * be preserved and passed back to the new instance.
 * </p>
 * <p>
 * I had considered using RxJava or RxKotlin, but learning what has a reputation for being a
 * complicated API didn't appeal. Besides, I've spent long enough on this test so far.
 * </p>
 * <p>
 * I also saw a reference to Volley. Again, I'd need to learn a new API, and I'm not sure how that
 * handles screen rotations either.
 * </p>
 * @author Graeme Sutherland
 * @since 06/03/2018
 * @param name A name [String] for the service, used to identify it during debugging
 * @param aConverter A [Converter] used to translate the downloaded data into an object ot type [T]
 */
abstract class SyncService<in T : Parcelable> (name: String, aConverter: Converter<T>)
	: IntentService(name) {

	/**
	 * Constants for passing values to and from the [SyncService].
	 */
	companion object {

		/**
		 * The source to download data from. This can be a filename, URL, or resource ID.
		 */
		const val SOURCE = "source"

		/**
		 * The type of data source.
		 */
		const val SOURCE_TYPE = "source_type"

		/**
		 * Download data from a URL.
		 */
		const val URL = "url"

		/**
		 * Read data from a file.
		 */
		const val FILE = "file"

		/**
		 * Read data from a resource.
		 */
		const val RESOURCE = "resource"

		/**
		 * Action identifying that an [Intent] is being sent with data in it.
		 */
		const val DATA_ACTION = "com.kizio.androidtest.SyncService.DATA"

		/**
		 * Key for retrieving data from a result.
		 */
		const val DATA = "data"
	}

	/**
	 * The [Converter] used to translate JSON into an object of type [T].
	 */
	private val converter = aConverter

 	/**
	 * Invoked when the service is triggered.
	 *
	 * @param intent The [Intent] used to start the service
	 */
	override fun onHandleIntent(intent: Intent?) {
		if (intent != null) {
			when(intent.getStringExtra(SOURCE_TYPE)) {
				URL -> onReceiveData(Retriever.download(intent.getStringExtra(SOURCE)))

				FILE -> onReceiveData(Retriever.readFile(intent.getStringExtra(SOURCE)))

				RESOURCE -> onReceiveData(Retriever.readResource(this,
						intent.getIntExtra(SOURCE, 0)))
			}
		}
	}

	/**
	 * Placeholder function to allow for post download processing of the data. Override this if you
	 * need a post-download processing step.
	 *
	 * @param data The downloaded data, now converted into an object of type [T]
	 */
	protected open fun process(data : T) {
		// Do nothing.
	}

	/**
	 * Invoked when the app receives data.
	 *
	 * @param json The downloaded data as a JSON [String]
	 */
	private fun onReceiveData(json: String) {
		val manager = LocalBroadcastManager.getInstance(this)
		val intent = Intent(DATA_ACTION)
		val data = converter.convert(json)

		// Additional processing of the data, as necessary.
		process(data)

		// Remember to return the data to the calling Activity. :) (Yes, I forgot this stage!)
		intent.putExtra(DATA, data)

		manager.sendBroadcast(intent)
	}
}