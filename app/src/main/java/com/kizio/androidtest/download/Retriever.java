package com.kizio.androidtest.download;

import android.content.Context;
import android.support.annotation.NonNull;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Helper methods for retrieving a {@link String} from a specified resource.
 * <p>
 * This class is based on a piece of code I used to download some JSON from a Twitter API. I'm using
 * it to save myself time putting something identical together. Needless to say, it's been extended
 * and rewritten, so I could have started from scratch...
 * <p></p>
 * I tried to convert it to Kotlin, but the tools messed it up, so it's staying in Java.
 * <p></p>
 * The class name isn't that descriptive, and it really needs something better.
 * </p>
 *
 * @author Graeme Sutherland
 * @since 26/01/2018
 */
public enum Retriever {;

	/**
	 * Downloads a {@link String} from the server at the specified URL.
	 *
	 * @param urlString A {@link String} containing the address to download the data from
	 * @return A {@link String} containing the server's response
	 * @throws IOException if there's an IO error accessing the server
	 */
	public static String download(@NonNull final String urlString) throws IOException {
		final URL url = new URL(urlString);
		final HttpURLConnection connection = (HttpURLConnection) url.openConnection ();

		try
		{
			connection.connect ();

			try (final InputStream stream = connection.getInputStream()) {
				return readFromStream(stream);
			}
		}

		finally
		{
			connection.disconnect ();
		}
	}

	/**
	 * Reads the contents of a file specified by the passed-in filename.
	 * <p>
	 * This method was written to read the contents of a local file, but I ended up using a raw
	 * resource, which simplifies the code. I'll probably delete it eventually, but it doesn't hurt
	 * to leave it here for now.
	 * </p>
	 *
	 * @param filename A {@link String} containing the name of the file to open
	 * @return The contents of the file as a {@link String}
	 * @throws IOException If there is an IO error during the read process
	 */
	@SuppressWarnings("unused")
	public static String readFile (@NonNull final String filename) throws IOException {
		return readFile(new File (filename));
	}

	/**
	 * Reads the contents of a file specified by the passed-in {@link File} object.
	 *
	 * @param file The {@link File} to open
	 * @return The contents of the file as a {@link String}
	 * @throws IOException If there is an IO error during the read process
	 */
	public static String readFile (@NonNull final File file) throws IOException {
		try (final FileReader reader = new FileReader(file)) {
			return readFromReader(reader);
		}
	}

	/**
	 * Reads the contents of a resource file.
	 *
	 * @param context The {@link Context} the app is running in
	 * @param id The ID of the raw resource to open
	 * @return A {@link String} containing the contents of the resource file
	 * @throws IOException If there is an IO error during the read process
	 */
	public static String readResource (@NonNull final Context context, final int id)
			throws IOException {
		try (final InputStream stream = context.getResources().openRawResource(id)) {
			return readFromStream(stream);
		}
	}

	/**
	 * Reads a {@link String} from the supplied {@link InputStream}.
	 *
	 * @param stream The {@link InputStream} that contains the data to be read
	 * @return A {@link String} containing the read data
	 * @throws IOException If there is an IO error during the read process
	 */
	private static String readFromStream(final InputStream stream) throws IOException {
		try (final InputStreamReader reader = new InputStreamReader(stream)) {
			return readFromReader(reader);
		}
	}

	/**
	 * Reads a {@link String} from the supplied {@link Reader}.
	 *
	 * @param reader The {@link Reader} that contains the data to be read
	 * @return A {@link String} containing the read data
	 * @throws IOException If there is an IO error during the read process
	 */
	private static String readFromReader(final Reader reader) throws IOException {
		try (final BufferedReader bufferedReader = new BufferedReader(reader)) {
			final StringBuilder builder = new StringBuilder();
			String line;

			while ((line = bufferedReader.readLine()) != null) {
				if (builder.length() > 0) {
					builder.append('\n');
				}

				builder.append(line);
			}

			return builder.toString();
		}
	}
}
