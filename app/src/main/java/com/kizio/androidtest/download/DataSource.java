package com.kizio.androidtest.download;

import java.io.IOException;

/**
 * This interface is intended to be used as a wrapper around a data source, allowing it to be passed
 * into a different function.
 * <p>
 * The app is based around the concept of using a
 * </p>
 * @author Graeme Sutherland
 * @since 06/03/2018
 */
public interface DataSource {

	/**
	 * Calling this will retrieve a piece of data from an endpoint.
	 *
	 * @return A {@link String} containing JSON from a data source
	 * @throws IOException If there was an error retrieving the data
	 */
	String retrieve() throws IOException;
}
