package com.kizio.androidtest.conversion;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.kizio.androidtest.data.Statement;
import com.kizio.androidtest.data.Transaction;

import java.lang.reflect.Type;
import java.math.BigInteger;

/**
 * JSON converter with custom calls to support the CBA JSON file.
 *
 * @author Graeme Sutherland
 * @since 06/03/2018
 */
public class CbaConverter extends Converter<Statement> {

	/**
	 * Constructor.
	 */
	public CbaConverter() {
		super(Statement.class);
	}

	/**
	 * Initialises custom code for parsing JSON for the CBA code test.
	 *
	 * @param builder The {@link GsonBuilder} being configured
	 */
	protected void initialiseBuilder(final GsonBuilder builder) {
		builder.setDateFormat(Transaction.DATE_FORMAT);
		builder.registerTypeAdapter(BigInteger.class, new BigIntegerHexDeserializer());
		builder.registerTypeAdapter(LatLng.class, new LocationDeserializer());
	}

	/**
	 * Provides a custom method to convert a hexadecimal {@link String} into a {@link BigInteger}.
	 */
	private static class BigIntegerHexDeserializer implements JsonDeserializer<BigInteger> {
		/**
		 * Parses a piece of JSON in a hex format to create a {@link BigInteger}.
		 *
		 * @param json The {@link JsonElement} being processed
		 * @param type The {@link Type} to be extracted
		 * @param context The {@link JsonDeserializationContext} the call is being made in
		 * @return A {@link BigInteger} derived from a hex value
		 * @throws JsonParseException If there is an error in the JSON
		 */
		@Override
		public BigInteger deserialize(final JsonElement json, final Type type,
									  final JsonDeserializationContext context)
				throws JsonParseException {
			final String hex = json.getAsString();

			return new BigInteger(hex, 16);
		}
	}

	/**
	 * Provides a custom method to convert a pair of location values into a {@link LatLng}.
	 */
	private static class LocationDeserializer implements JsonDeserializer<LatLng> {
		/**
		 * [String] containing the latitude field in the JSON file.
		 */
		private static final String LATITUDE = "lat";

		/**
		 * [String] containing the longitude field in the JSON file.
		 */
		private static final String LONGITUDE = "lng";

		/**
		 * Parses a piece of JSON to create a {@link LatLng}.
		 *
		 * @param json The {@link JsonElement} being processed
		 * @param type The {@link Type} to be extracted
		 * @param context The {@link JsonDeserializationContext} the call is being made in
		 * @return A {@link LatLng} derived from the supplied JSON
		 * @throws JsonParseException If there is an error in the JSON
		 */
		@Override
		public LatLng deserialize(final JsonElement json, final Type type,
								  final JsonDeserializationContext context)
				throws JsonParseException {
			final JsonObject object = json.getAsJsonObject();
			final double latitude = object.get(LATITUDE).getAsDouble();
			final double longitude = object.get(LONGITUDE).getAsDouble();

			return new LatLng(latitude, latitude);
		}
	}
}
