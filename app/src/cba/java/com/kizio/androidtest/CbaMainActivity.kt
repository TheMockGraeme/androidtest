package com.kizio.androidtest

import android.content.Intent
import android.os.Bundle
import com.kizio.androidtest.adapter.TransactionAdapter
import com.kizio.androidtest.data.Statement
import com.kizio.androidtest.listener.BlockGroupClickListener
import com.kizio.androidtest.sync.CbaSyncService
import com.kizio.androidtest.sync.SyncService
import com.kizio.androidtest.view.AccountView
import kotlinx.android.synthetic.cba.activity_cba.transaction_list

/**
 * The main activity for the CBA test app.
 *
 * @author Graeme Sutherland
 * @since 04/03/2018
 */
class CbaMainActivity : MainActivity<Statement>() {

	/**
	 * Invoked when the app is created.
	 */
	override fun onCreate(savedInstanceState: Bundle?) {
		// The content view has to be setup before onCreate is called, otherwise there might be a
		// null exception when the app attempts to restore the data field.
		setContentView(R.layout.activity_cba)

		super.onCreate(savedInstanceState)

		// Sets up a listener on the group to prevent it from being collapsed.
		transaction_list.setOnGroupClickListener(BlockGroupClickListener())
	}

	/**
	 * Gets the class to launch the [SyncService].
	 *
	 * @return The [SyncService] class that the activity uses
	 */
	override fun getSyncServiceClass(): Class<out SyncService<Statement>> {
		return CbaSyncService::class.java
	}

	/**
	 * Sets the [Intent] used to start the [SyncService].
	 *
	 * @param intent The [Intent] to configure
	 */
	override fun initialiseIntent(intent: Intent) {
		// intent.putExtra(SyncService.SOURCE_TYPE, SyncService.RESOURCE)
		// intent.putExtra(SyncService.SOURCE, R.raw.exercise)

		// TODO The source string needs to be externalised.
		intent.putExtra(SyncService.SOURCE_TYPE, SyncService.URL)
		intent.putExtra(SyncService.SOURCE, "https://www.dropbox.com/s/tewg9b71x0wrou9/data.json?dl=1")
	}

	/**
	 * Invoked when the data to display is set.
	 */
	override fun onSetData(newData: Statement?) {
		if (newData != null && transaction_list != null) {
			val transactions = newData.getGroupedTransactions()
			val adapter = TransactionAdapter(this, transactions)
			val header = AccountView(this)
			var i = 0

			header.setAccount(newData.account)

			transaction_list.addHeaderView(header,null, false)
			transaction_list.setAdapter(adapter)

			while (i < adapter.groupCount) {
				transaction_list.expandGroup(i++)
				transaction_list.setGroupIndicator(null)
			}
		}
	}
}
