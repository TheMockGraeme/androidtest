package com.kizio.androidtest.adapter

import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.BaseExpandableListAdapter
import android.widget.TextView
import com.kizio.androidtest.data.Statement
import com.kizio.androidtest.data.Transaction
import com.kizio.androidtest.view.TransactionView
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale

/**
 * Displays the contents of an account statement.
 *
 * @author Graeme Sutherland
 * @since 07/03/2018
 *
 * @param aContext The [Context] the adapter is being displayed in
 * @param transactionMap A [Map] containing the data to display
 */
class TransactionAdapter(aContext: Context, transactionMap: Map<Date, List<Transaction>>)
	: BaseExpandableListAdapter() {

	/**
	 * The [Context] the app is being displayed in. Used for creating new [View] objects
	 */
	private val context = aContext

	/**
	 * The [Statement] object being displayed by the adapter.
	 */
	private val transactions = transactionMap

	/**
	 * The keys to the [Map], converted into an array.
	 */
	private val keys = transactionMap.keys.toTypedArray()

	/**
	 * Gets the data associated with the given group.
	 *
	 * @param groupPosition The position of the group
	 * @return The [List] of [Transaction] objects corresponding to the group
	 */
	override fun getGroup(groupPosition: Int): Date {
		return keys[groupPosition]
	}

	/**
	 * Gets whether the child at the specified position is selectable.
	 *
	 * @param groupPosition The position of the group that contains the child
	 * @param childPosition The position of the child within the group
	 * @return Always returns false
	 */
	override fun isChildSelectable(groupPosition: Int, childPosition: Int): Boolean {
		return false
	}

	/**
	 * Indicates whether the child and group IDs are stable across changes to the underlying data.
	 *
	 * @return Always returns false
	 */
	override fun hasStableIds(): Boolean {
		return false
	}

	/**
	 * Gets a View that displays the given group.
	 *
	 * @param groupPosition The position of the group for which the View is returned
	 * @param isExpanded Whether the group is expanded or collapsed
	 * @param convertView The [View] to reuse if possible
	 * @param parent The parent [ViewGroup] that this view will eventually be attached to
	 * @return The [View] to display at the specified position
	 */
	override fun getGroupView(groupPosition: Int, isExpanded: Boolean, convertView: View?,
							  parent: ViewGroup?)
			: View {
		val dateView: TextView
		val date = getGroup(groupPosition)
		val format = SimpleDateFormat(Transaction.DATE_FORMAT, Locale.UK)

		if (convertView is TextView) {
			dateView = convertView
		} else {
			dateView = TextView(context)
		}

		dateView.text = format.format(date)
		dateView.setBackgroundColor(0xffffcc00.toInt())

		return dateView
	}

	/**
	 * Gets the number of children in a specified group.
	 *
	 * @param groupPosition The position of the group
	 * @return The number of children in the group
	 */
	override fun getChildrenCount(groupPosition: Int): Int {
		return getChildren(groupPosition)?.size ?: 0
	}

	/**
	 * Gets the data associated with the given child within the given group.
	 *
	 * @param groupPosition The position of the group that the child resides in
	 * @param childPosition The position of the child within the group
	 * @return The data corresponding to the child
	 */
	override fun getChild(groupPosition: Int, childPosition: Int): Transaction? {
		return getChildren(groupPosition)?.get(childPosition)
	}

	/**
	 * Gets the ID for the group at the given position.
	 *
	 * @param groupPosition The position of the group for which the ID is wanted
	 * @return The ID associated with the group
	 */
	override fun getGroupId(groupPosition: Int): Long {
		return groupPosition.toLong().shl(32)
	}

	/**
	 * Gets a [View] that displays the data for the given child within the given group.
	 *
	 * @param groupPosition The position of the group that contains the child
	 * @param childPosition The position of the child within the group
	 * @param isLastChild Whether the child is the last child within the group
	 * @param convertView The [View] to reuse if possible
	 * @param parent The parent [ViewGroup] that this view will eventually be attached to
	 * @return The [View] corresponding to the child at the specified position
	 */
	override fun getChildView(groupPosition: Int, childPosition: Int, isLastChild: Boolean, convertView: View?, parent: ViewGroup?): View {
		val transactionView: TransactionView
		val transaction = getChild(groupPosition, childPosition)

		if (convertView is TransactionView) {
			transactionView = convertView
		} else {
			transactionView = TransactionView(context)
		}

		transactionView.setTransaction(transaction)

		return transactionView
	}

	/**
	 * Gets the ID for the given child within the given group.
	 *
	 * @param groupPosition The position of the group that contains the child
	 * @param childPosition The position of the child within the group
	 * @return The ID associated with the child
	 */
	override fun getChildId(groupPosition: Int, childPosition: Int): Long {
		return getGroupId(groupPosition) + childPosition
	}

	/**
	 * Gets the number of groups.
	 *
	 * @return The number of groups
	 */
	override fun getGroupCount(): Int {
		return keys.size
	}

	/**
	 * Helper method to get the children of a group at a specific position
	 *
	 * @param groupPosition The position of the group that contains the children
	 * @return A [List] of [Transaction] objects containing the children
	 */
	private fun getChildren(groupPosition: Int) : List<Transaction>? {
		return transactions[keys[groupPosition]]
	}
}
