package com.kizio.androidtest.view

import android.content.Context
import android.util.AttributeSet
import android.widget.LinearLayout
import com.kizio.androidtest.R
import com.kizio.androidtest.data.Account
import kotlinx.android.synthetic.cba.view_account.view.account_balance
import kotlinx.android.synthetic.cba.view_account.view.account_number
import kotlinx.android.synthetic.cba.view_account.view.account_type
import kotlinx.android.synthetic.cba.view_account.view.available_funds

/**
 * Displays the details of an account.
 *
 * @author Graeme Sutherland
 * @since 08/03/2018
 * @param context The [Context] the view is being displayed in
 * @param attributes The [AttributeSet] used to style the view
 * @param defaultStyle The default style to apply to the view
 */
class AccountView (context: Context?, attributes: AttributeSet?, defaultStyle: Int)
	: LinearLayout(context, attributes, defaultStyle) {

	/**
	 * Alternative constructor.
	 *
	 * @param context The [Context] the view is being displayed in
	 */
	constructor(context: Context?) : this(context, null)

	/**
	 * Alternative constructor.
	 *
	 * @param context The [Context] the view is being displayed in
	 * @param attributes The [AttributeSet] used to style the view
	 */
	constructor(context: Context?, attributes: AttributeSet?) : this(context, attributes, 0)

	/**
	 * Initialisation block.
	 */
	init {
		inflate(context, R.layout.view_account, this)

		orientation = VERTICAL
	}

	/**
	 * Sets the [Account] data to display.
	 *
	 * @param account The [Account] that contains the data to display
	 */
	public fun setAccount(account: Account?) {
		if (account != null) {
			account_type.text = account.name
			account_number.text = account.number
			available_funds.text = context.getString(R.string.available_funds, account.available.toString())
			account_balance.text = context.getString(R.string.account_balance, account.balance.toString())
		} else {
			account_type.text = null
			account_number.text = null
			available_funds.text = null
			account_balance.text = null
		}
	}
}