package com.kizio.androidtest.view

import android.content.Context
import android.text.Html
import android.util.AttributeSet
import android.view.View
import android.widget.LinearLayout
import com.kizio.androidtest.R
import com.kizio.androidtest.data.PendingTransaction
import com.kizio.androidtest.data.Transaction
import kotlinx.android.synthetic.cba.view_transaction.view.amount
import kotlinx.android.synthetic.cba.view_transaction.view.description
import kotlinx.android.synthetic.cba.view_transaction.view.pending

/**
 * Displays the contents of a transaction.
 *
 * @author Graeme Sutherland
 * @since 07/03/2018
 * @param context The [Context] the view is being displayed in
 * @param attributes The [AttributeSet] used to style the view
 * @param defaultStyle The default style to apply to the view
 */
class TransactionView (context: Context?, attributes: AttributeSet?, defaultStyle: Int)
	: LinearLayout(context, attributes, defaultStyle) {

	/**
	 * Alternative constructor.
	 *
	 * @param context The [Context] the view is being displayed in
	 */
	constructor(context: Context?) : this(context, null)

	/**
	 * Alternative constructor.
	 *
	 * @param context The [Context] the view is being displayed in
	 * @param attributes The [AttributeSet] used to style the view
	 */
	constructor(context: Context?, attributes: AttributeSet?) : this(context, attributes, 0)

	/**
	 * Initialisation block.
	 */
	init {
		inflate(context, R.layout.view_transaction, this)
	}

	/**
	 * Sets the [Transaction] data to display in the view.
	 *
	 * @param transaction The [Transaction] that contains the data to display
	 */
	public fun setTransaction(transaction: Transaction?) {
		pending.visibility = if (transaction is PendingTransaction) {
			View.VISIBLE
		} else {
			View.GONE
		}

		if (transaction != null) {
			description.text = Html.fromHtml(transaction.description)
			amount.text = transaction.amount.toString()
		} else {
			description.text = null
			amount.text = null
		}
	}
}