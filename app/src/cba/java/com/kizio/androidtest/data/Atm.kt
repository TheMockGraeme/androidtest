package com.kizio.androidtest.data

import android.os.Parcel
import android.os.Parcelable
import com.google.android.gms.maps.model.LatLng
import com.google.gson.annotations.SerializedName

/**
 * Holds the values for an ATM.
 *
 * @author Graeme Sutherland
 * @since 04/03/2018
 */
class Atm() : Parcelable {

	/**
	 * Constants for parsing the JSON returned file, and recreating the object from a [Parcel].
	 */
	private companion object CREATOR : Parcelable.Creator<Atm>  {

		/**
		 * Creates an instance of an [Atm] object from a [Parcel].
		 *
		 * @param parcel The [Parcel] the [Atm] is to be created from
		 * @return The newly created [Atm] object
		 */
		override fun createFromParcel(parcel: Parcel): Atm {
			return Atm(parcel)
		}

		/**
		 * Creates an empty [Array] of [Atm] objects of the specified size.
		 *
		 * @param size The size of the new array
		 * @return An empty [Array] of [Atm] objects
		 */
		override fun newArray(size: Int): Array<Atm?> {
			return arrayOfNulls(size)
		}

		/**
		 * [String] containing the ID field in the JSON file.
		 */
		private const val ID : String = "id"

		/**
		 * [String] containing the name field in the JSON file.
		 */
		private const val NAME : String = "name"

		/**
		 * [String] containing the amount spent field in the JSON file.
		 */
		private const val ADDRESS : String = "address"

		/**
		 * [String] containing the effective date field in the JSON file.
		 */
		private const val LOCATION : String = "location"
	}

	/**
	 * The ID [Int] for the ATM.
	 */
	@SerializedName(ID)
	var id : Int = 0

	/**
	 * The name [String] for the ATM.
	 */
	@SerializedName(NAME)
	var name : String = ""

	/**
	 * The address [String] for the ATM.
	 */
	@SerializedName(ADDRESS)
	var address : String = ""

	/**
	 * An ID number for an ATM. The JSON file doesn't include what format it's in, so I'm assuming
	 * that an [Int] will be fine.
	 */
	@SerializedName(LOCATION)
	var location = LatLng(0.0, 0.0)

	/**
	 * Constructor for creating an instance of the object from a [Parcel].
	 *
	 * @param parcel The [Parcel] that contains the object's details
	 */
	constructor(parcel: Parcel) : this() {
		id = parcel.readInt()
		name = parcel.readString()
		address = parcel.readString()
		location = parcel.readParcelable<LatLng>(LatLng::class.java.classLoader)
	}

	/**
	 * Writes the contents of the object to a [Parcel].
	 *
	 * @param parcel The [Parcel] to write to
	 * @param flags Describes how the object should be written
	 */
	override fun writeToParcel(parcel: Parcel, flags: Int) {
		parcel.writeInt(id)
		parcel.writeString(name)
		parcel.writeString(address)
		parcel.writeParcelable(location, flags)
	}

	/**
	 * Indicates whether the class needs any special processing.
	 *
	 * @return Always returns 0
	 */
	override fun describeContents(): Int {
		return 0
	}
}
