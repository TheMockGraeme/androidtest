package com.kizio.androidtest.data

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import java.math.BigDecimal
import java.math.BigInteger
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale

/**
 * Holds the values for a line in an account statement.
 *
 * @author Graeme Sutherland
 * @since 26/01/2018
 */
open class Transaction () : Comparable<Transaction>, Parcelable {

	/**
	 * Constants for parsing the JSON returned file, and recreating the object from a [Parcel].
	 */
	companion object CREATOR : Parcelable.Creator<Transaction>  {

		/**
		 * Creates an instance of an [Transaction] object from a [Parcel].
		 *
		 * @param parcel The [Parcel] the [Transaction] is to be created from
		 * @return The newly created [Transaction] object
		 */
		override fun createFromParcel(parcel: Parcel): Transaction {
			return Transaction(parcel)
		}

		/**
		 * Creates an empty [Array] of [Transaction] objects of the specified size.
		 *
		 * @param size The size of the new array
		 * @return An empty [Array] of [Transaction] objects
		 */
		override fun newArray(size: Int): Array<Transaction?> {
			return arrayOfNulls(size)
		}

		/**
		 * Date format [String] used by the app.
		 */
		const val DATE_FORMAT = "dd/MM/yyyy"

		/**
		 * [String] containing the ID field in the JSON file.
		 */
		private const val ID : String = "id"

		/**
		 * [String] containing the description field in the JSON file.
		 */
		private const val DESCRIPTION : String = "description"

		/**
		 * [String] containing the amount spent field in the JSON file.
		 */
		private const val AMOUNT : String = "amount"

		/**
		 * [String] containing the effective date field in the JSON file.
		 */
		private const val DATE : String = "effectiveDate"

		/**
		 * [String] containing the ATM ID field in the JSON file.
		 */
		private const val ATM_ID : String = "atmId"
	}

	/**
	 * A [SimpleDateFormat] object used to convert a date object into a human readable format.
	 */
	private val displayFormat = SimpleDateFormat(DATE_FORMAT, Locale.UK)

	/**
	 * The ID for the statement item. This is an alphanumeric [String] in the data file, which
	 * appears to correspond to a 128 bit [BigInteger].
	 */
	@SerializedName(ID)
	var id : BigInteger = BigInteger.ZERO

	/**
	 * A description [String] of what was purchased.
	 */
	@SerializedName(DESCRIPTION)
	var description : String = ""

	/**
	 * Floating point values aren't ideal for storing currencies due to rounding errors, so the
	 * amount of money is a [BigDecimal].
	 */
	@SerializedName(AMOUNT)
	var amount : BigDecimal = BigDecimal.ZERO

	/**
	 * The [Date] that the transaction was made on.
	 */
	@SerializedName(DATE)
	var date : Date = Date(0)

	/**
	 * An ID number for an ATM. The JSON file doesn't include what format it's in, so I'm assuming
	 * that an [Int] will be fine.
	 */
	@SerializedName(ATM_ID)
	var atmId : Int? = null

	/**
	 * Gets the [BigInteger] ID value as a hexadecimal [String].
	 *
	 * @return The ID as a [String]
	 */
	fun getFormattedIdString() : String {
		return id.toString(16)
	}

	/**
	 * Formats the date into a [String] with the form DD/MM/YYYY.
	 *
	 * @return The formatted [Date] as a [String]
	 */
	fun getFormattedDateString() : String {
		return displayFormat.format(date)
	}

	/**
	 * Constructor for creating an instance of the object from a [Parcel].
	 *
	 * @param parcel The [Parcel] that contains the object's details
	 */
	constructor(parcel: Parcel) : this() {
		readFromParcel(parcel)
	}

	/**
	 * Helper method that reads values from a [Parcel].
	 *
	 * @param parcel The [Parcel] that contains the object's details
	 */
	protected fun readFromParcel(parcel: Parcel) {
		id = parcel.readSerializable() as BigInteger
		description = parcel.readString()
		amount = parcel.readSerializable() as BigDecimal
		date = parcel.readSerializable() as Date
		atmId = parcel.readSerializable() as Int
	}
	/**
	 * Writes the contents of the object to a [Parcel].
	 *
	 * @param parcel The [Parcel] to write to
	 * @param flags Describes how the object should be written
	 */
	override fun writeToParcel(parcel: Parcel, flags: Int) {
		parcel.writeSerializable(id)
		parcel.writeString(description)
		parcel.writeSerializable(amount)
		parcel.writeSerializable(date)
		parcel.writeSerializable(atmId)
	}

	/**
	 * Indicates whether the class needs any special processing.
	 *
	 * @return Always returns 0
	 */
	override fun describeContents(): Int {
		return 0
	}

	/**
	 * Compares this [Transaction] to another. This is based on the embedded [Date] object.
	 *
	 * @return 0 if they're equal, negative if it's less than the [other], positive if greater
	 */
	override fun compareTo(other: Transaction): Int {
		val dataComparison = date.compareTo(other.date)
		val comparison : Int

		if (dataComparison != 0) {
			comparison = dataComparison
		} else {
			comparison = id.compareTo(other.id)
		}

		return comparison
	}
}