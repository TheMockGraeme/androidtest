package com.kizio.androidtest.data

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import java.util.Date
import java.util.TreeMap

/**
 * Holds the collection values that make up the statement record. This is an [Account],
 * [Transaction] arrays for pending and performed transactions, and an [Atm] array showing which
 * were used for withdrawals.
 *
 * @author Graeme Sutherland
 * @since 04/03/2018
 */
class Statement() : Parcelable {

	/**
	 * Constants for parsing the JSON returned file, and recreating the object from a [Parcel].
	 */
	private companion object CREATOR : Parcelable.Creator<Statement>  {
		/**
		 * Creates an instance of an [Statement] object from a [Parcel].
		 *
		 * @param parcel The [Parcel] the [Statement] is to be created from
		 * @return The newly created [Statement] object
		 */
		override fun createFromParcel(parcel: Parcel): Statement {
			return Statement(parcel)
		}

		/**
		 * Creates an empty [Array] of [Statement] objects of the specified size.
		 *
		 * @param size The size of the new array
		 * @return An empty [Array] of [Statement] objects
		 */
		override fun newArray(size: Int): Array<Statement?> {
			return arrayOfNulls(size)
		}

		/**
		 * [String] containing the account field in the JSON file.
		 */
		private const val ACCOUNT : String = "account"

		/**
		 * [String] containing the transactions field in the JSON file.
		 */
		private const val TRANSACTIONS : String = "transactions"

		/**
		 * [String] containing the pending transactions field in the JSON file.
		 */
		private const val PENDING : String = "pending"

		/**
		 * [String] containing the ATMs used field in the JSON file.
		 */
		private const val ATM : String = "atms"
	}

	/**
	 * The [Account] for the statement.
	 */
	@SerializedName(ACCOUNT)
	var account : Account = Account()

	/**
	 * The completed transactions, as an array of [Transaction] objects.
	 */
	@SerializedName(TRANSACTIONS)
	private var transactions = arrayOf<Transaction> ()

	/**
	 * The pending transactions, as an array of [Transaction] objects.
	 */
	@SerializedName(PENDING)
	private var pending = arrayOf<PendingTransaction> ()

	/**
	 * The ATMs used for withdrawals, as an array of [Atm] objects.
	 */
	@SerializedName(ATM)
	var atms = arrayOf<Atm> ()

	/**
	 * Constructor for creating an instance of the object from a [Parcel].
	 *
	 * @param parcel The [Parcel] that contains the object's details
	 */
	@Suppress("UNCHECKED_CAST")
	constructor(parcel: Parcel) : this() {
		account = parcel.readParcelable(Account::class.java.classLoader)
		transactions = parcel.readArray(Transaction::class.java.classLoader) as Array<Transaction>
		pending = parcel.readArray(PendingTransaction::class.java.classLoader) as Array<PendingTransaction>
		atms = parcel.readArray(Array<Atm>::class.java.classLoader) as Array<Atm>
	}

	/**
	 * Writes the contents of the object to a [Parcel].
	 *
	 * @param parcel The [Parcel] to write to
	 * @param flags Describes how the object should be written
	 */
	override fun writeToParcel(parcel: Parcel, flags: Int) {
		parcel.writeParcelable(account, flags)
		parcel.writeParcelableArray(transactions, flags)
		parcel.writeParcelableArray(pending, flags)
		parcel.writeParcelableArray(atms, flags)
	}

	/**
	 * Indicates whether the class needs any special processing.
	 *
	 * @return Always returns 0
	 */
	override fun describeContents(): Int {
		return 0
	}

	/**
	 * Gets a [Map] which groups the [Transaction] objects together by their [Date].
	 *
	 * @return A [Map] that groups [Transaction] records by [Date]
	 */
	fun getGroupedTransactions() : Map<Date, List<Transaction>> {
		val map : TreeMap<Date, ArrayList<Transaction>> = TreeMap()

		populateTransactionMap (map, transactions)

		populateTransactionMap (map, pending)

		// The ArrayList isn't sorted, so it's necessary to do this as a final step.
		// TODO convert to arrays?
		for (date: Date in map.keys) {
			map[date]?.sort()
		}

		return map
	}

	/**
	 * Extracts [Transaction] values from the stored [Array], and bundles them into buckets in the
	 * supplied [TreeMap].
	 * <p>
	 * This only works because the date is supplied in the mm/dd/yyyy format, and so comparisons
	 * aren't done on a millisecond granularity. If this changes, I'd need to convert the date into
	 * the day only.
	 * </p>
	 *
	 * @param map The [TreeMap] which is being used to store the bundled times and dates
	 * @param transactionArray An [Array] of [Transaction] objects to add to the map
	 */
	private fun <T : Transaction> populateTransactionMap(map : TreeMap<Date, ArrayList<Transaction>>,
														 transactionArray: Array<T>) {
		for (transaction: Transaction in transactionArray) {
			val datedTransactions : ArrayList<Transaction>

			if (map[transaction.date] == null) {
				datedTransactions = ArrayList()
				map[transaction.date] = datedTransactions
			} else {
				// The cast is necessary to remove the nullable condition.
				datedTransactions = map[transaction.date] as ArrayList<Transaction>
			}

			datedTransactions.add(transaction)
		}
	}
}
