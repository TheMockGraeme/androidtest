package com.kizio.androidtest.data

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import java.math.BigDecimal

/**
 * Holds the values for an Account.
 *
 * @author Graeme Sutherland
 * @since 04/03/2018
 */
class Account() : Parcelable {

	/**
	 * Constants for parsing the JSON returned file, and recreating the object from a [Parcel].
	 */
	private companion object CREATOR : Parcelable.Creator<Account>  {

		/**
		 * Creates an instance of an [Account] object from a [Parcel].
		 *
		 * @param parcel The [Parcel] the [Account] is to be created from
		 * @return The newly created [Account] object
		 */
		override fun createFromParcel(parcel: Parcel): Account {
			return Account(parcel)
		}

		/**
		 * Creates an empty [Array] of [Account] objects of the specified size.
		 *
		 * @param size The size of the new array
		 * @return An empty [Array] of [Account] objects
		 */
		override fun newArray(size: Int): Array<Account?> {
			return arrayOfNulls(size)
		}

		/**
		 * [String] containing the account name field in the JSON file.
		 */
		private const val NAME : String = "accountName"

		/**
		 * [String] containing the account number field in the JSON file.
		 */
		private const val NUMBER : String = "accountNumber"

		/**
		 * [String] containing the available account balance in the JSON file.
		 */
		private const val AVAILABLE : String = "available"

		/**
		 * [String] containing the actual account balance in the JSON file.
		 */
		private const val BALANCE : String = "balance"
	}

	/**
	 * The account name [String] for the ATM
	 */
	@SerializedName(NAME)
	var name : String = ""

	/**
	 * The account number [String] for the ATM.
	 */
	@SerializedName(NUMBER)
	var number : String = ""

	/**
	 * The available balance as a [BigDecimal].
	 */
	@SerializedName(AVAILABLE)
	var available : BigDecimal = BigDecimal(0)

	/**
	 * The actual account balance as a [BigDecimal].
	 */
	@SerializedName(BALANCE)
	var balance : BigDecimal = BigDecimal(0)

	/**
	 * Constructor for creating an instance of the object from a [Parcel].
	 *
	 * @param parcel The [Parcel] that contains the object's details
	 */
	constructor(parcel: Parcel) : this() {
		name = parcel.readString()
		number = parcel.readString()
	}

	/**
	 * Writes the contents of the object to a [Parcel].
	 *
	 * @param parcel The [Parcel] to write to
	 * @param flags Describes how the object should be written
	 */
	override fun writeToParcel(parcel: Parcel, flags: Int) {
		parcel.writeString(name)
		parcel.writeString(number)
	}

	/**
	 * Indicates whether the class needs any special processing.
	 *
	 * @return Always returns 0
	 */
	override fun describeContents(): Int {
		return 0
	}
}
