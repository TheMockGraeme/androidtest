package com.kizio.androidtest.data

import android.os.Parcel
import android.os.Parcelable

/**
 * The [PendingTransaction] class allows the app to tag a [Transaction] as pending without setting a
 * flag.
 *
 * @author Graeme Sutherland
 * @since 05/03/2018
 */
class PendingTransaction () : Transaction () {
	/**
	 * Constants for parsing the JSON returned file, and recreating the object from a [Parcel].
	 */
	private companion object CREATOR : Parcelable.Creator<PendingTransaction> {

		/**
		 * Creates an instance of an [PendingTransaction] object from a [Parcel].
		 *
		 * @param parcel The [Parcel] the [PendingTransaction] is to be created from
		 * @return The newly created [PendingTransaction] object
		 */
		override fun createFromParcel(parcel: Parcel): PendingTransaction {
			return PendingTransaction(parcel)
		}

		/**
		 * Creates an empty [Array] of [PendingTransaction] objects of the specified size.
		 *
		 * @param size The size of the new array
		 * @return An empty [Array] of [PendingTransaction] objects
		 */
		override fun newArray(size: Int): Array<PendingTransaction?> {
			return arrayOfNulls(size)
		}    // Empty class.
	}

	/**
	 * Constructor for creating an instance of the object from a [Parcel].
	 *
	 * @param parcel The [Parcel] that contains the object's details
	 */
	constructor(parcel: Parcel) : this() {
		readFromParcel(parcel)
	}
}
