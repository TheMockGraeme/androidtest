package com.kizio.androidtest.sync

import com.kizio.androidtest.conversion.CbaConverter
import com.kizio.androidtest.data.Statement

/**
 * [SyncService] for the CBA code test app.
 *
 * @author Graeme Sutherland
 * @since 07/03/2018
 */
class CbaSyncService : SyncService<Statement> (CbaSyncService::class.java.simpleName, CbaConverter()) {
}
