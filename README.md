# AndroidTest
This is a generic solution for Android coding tests.

## Motivation
Most Android coding tests take me an awful lot longer than I think is reasonable. I'm typically told that I should be able to complete them within two to three hours, but in practice they seem to need anything up to several days. Being a lazy programmer, I want to cut down on the effort that I need to put in.

Fortunately most of these tests follow a standard pattern, though generally not all steps will be required:
- Download a piece of data from a webservice or other endpoint.
- Process the JSON into a format that can be used locally.
- Store this in a database.
- Display the data in a [`ListView`](https://developer.android.com/guide/topics/ui/layout/listview.html).
- Allow the user to interact with the displayed data.

By figuring out a more generic solution, I can hopefully reduce the amount of time it takes me to code up one of these. At the time of writing, I've got two tests that are due to be completed, and waiting for a third to be sent. This is probably a week's full-time work to clear.

## Approach
I'm going to use the following language and Gradle features:
- Object-orientated coding, leaning heavily on the `interface` and `abstract` keywords to allow a standard programming API to be used. This enables reuse of common components.
- Generic types to allow different classes to be plugged into the framework.
- Flavours in the Gradle build system allow individual files to be replaced for the targets.

### Advantages
The main advantage is that I should be able to trade off a bit more complexity and time spent in coding an initial solution against future problems.

If I'm using more advanced language features, it should also give me a chance to show off my knowledge a bit more.

### Disadvantages
There are a number of risks that I've identified:
- By making things more complicated there's a risk of introducing bugs.
- More complex code is harder to read, and this will make whoever is reviewing my solution think that I'm a worse programmer than I might be.
- I'm violating the [YAGNI](https://martinfowler.com/bliki/Yagni.html) principle, by gold plating my initial solution to cope with possible coding tests down the line.
- The code is primarily intended to reduce the time I spend on it, rather than being an optimal solution to your problem. Sorry.

## Source Code
The source is written in a mixture of Kotlin and Java.

Some of the files have been pulled from other projects, and reused, so remain in their original language.

In other cases, I've wanted to use a specific language feature that I know in Java. For example, using an `enum` to create a static library class.

### Structure
The source tree is structured as a standard Android / Gradle project, with additional flavours in the [src](app/src) directory.
- [androidTest](app/src/androidTest): Android test files.
- [capgemini](app/src/capgemini): Code and resources for Capgemini's test solution.
- [cba](app/src/cba): Code and resources for the CBA's test solution.
- [main](app/src/main): Common code and resources for all flavours.
- [test](app/src/test): Java test files.

## TODO
Neither the Capgemini nor CBA apps are complete.

### CBA
The app needs styling, and a lot of the data being displayed isn't correctly formatted.

I can make an [`ExpandableListView`](https://developer.android.com/reference/android/widget/ExpandableListView.html) jump through hoops if need be, but it's time consuming.

Take a look at the [recipe screen I wrote for the Paula Deen Network](http://www.kizio.com/2014/12/paula-deen-network.html), which consisted of a single [`ExpandableListView`](https://developer.android.com/reference/android/widget/ExpandableListView.html) with multiple styles applied to it.

From memory the composition was:
- The section from the photograph of Paula Deen down to the **Cook Now** and **+Favourites** buttons was the list's header.
- The **Ingredients**, **Preparation**, and **Reviews** corresponded to the group level of the list.
- The **Ingredients** section had two view styles, for the grey and white backgrounds.
- The **Preparation** list elements included both the **Step x** header, as well as the body below.
- The **Reviews** group view ran down to the **Sort by** dropdown. It had to handle to colour transition halfway through. Plus on the tablet version, there were thin bars down each side of the screen that also had to be created.
- From **You might also like** down was the list's footer. It reused a component that another developer had created.

This took me a couple of weeks to put together, but was more memory efficient (so ran smoother) than other activities which used a static layout.

I've not done the following extras:
- Some transactions will be ATM withdrawals. These rows should be indicated by the location icon.
- Tapping on an ATM withdrawal row will show the location of the ATM on a map. (The JSON location data is being imported as a Google Maps [`LatLng`](https://developers.google.com/android/reference/com/google/android/gms/maps/model/LatLng) object, so should be fairly easy to get the data in.)
- Implement an algorithm that will give the user a projected spend over the next two weeks.

### Capgemini
The main thing missing is that I've not implemented lazy downloading of images.

Were this a commercial project, I'd question the wisdom of doing this. If the app needs half a dozen thumbnails, it'd be easier to pull them down once the JSON has downloaded, rather than as the user brings them up. This would have two advantages:
1. The code would be simpler, which would mean less opportunity for bugs creeping in. It'd probably be more efficient too.
2. The user experience would be better, as it's likely images would be present by the time he or she needed them.

### General
There are a few things that I'd like to clear up in the app's design.

I'm not happy with the program flow in the [`SyncService`](app/src/main/java/com/kizio/androidtest/sync/SyncService.kt) class. I don't know how I'd rewrite it, but the [`process`](app/src/main/java/com/kizio/androidtest/sync/SyncService.kt#L103) method feels out of place.

Similarly, the setup code for an [`Intent`](https://developer.android.com/reference/android/content/Intent.html) in the [`MainActivity`](app/src/main/java/com/kizio/androidtest/MainActivity.kt) class isn't as neat as I'd like. I should roll the [`getSyncServiceClass`](app/src/main/java/com/kizio/androidtest/MainActivity.kt#L139) and [`initialiseIntent`](app/src/main/java/com/kizio/androidtest/MainActivity.kt#L148) into a single call.

In retrospect, I should have left out being able to read data from a file or a raw resource. The latter was used for testing in the CBA's app, but it adds complexity to the code, and probably won't be needed again.

Lastly, I wish that I'd known about [`@Parcelize`](https://android.jlelse.eu/yet-another-awesome-kotlin-feature-parcelize-5439718ba220) sooner. It removed a lot of boilerplate code in the Capgemini app, compared with the CBA one.
